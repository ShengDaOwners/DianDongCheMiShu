//
//  StatisticModel.h
//  GNETS
//
//  Created by fyc on 16/2/24.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatisticModel : NSObject

@property (nonatomic, retain)NSString *code;
@property (nonatomic, retain)NSString *avgSpeed;
@property (nonatomic, retain)NSString *carId;
@property (nonatomic, retain)NSString *date;
@property (nonatomic, retain)NSString *maxSpeed;
@property (nonatomic, retain)NSString *mileage;
@property (nonatomic, retain)NSString *minSpeed;
@property (nonatomic, retain)NSString *errmsg;
@property (nonatomic, retain)NSString *dayId;

- (id)initWithaDic:(NSDictionary *)aDic;

- (id)initWithArrAndDic:(NSDictionary *)aDic;

@end
