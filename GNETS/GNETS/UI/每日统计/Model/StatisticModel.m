//
//  StatisticModel.m
//  GNETS
//
//  Created by fyc on 16/2/24.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "StatisticModel.h"

@implementation StatisticModel

- (id)initWithaDic:(NSDictionary *)aDic{
    if (self == [super init]) {
        
        self.code = [NSString stringWithFormat:@"%@",aDic[@"code"]];
        self.errmsg = [NSString stringWithFormat:@"%@",aDic[@"errmsg"]];
        self.avgSpeed = [NSString stringWithFormat:@"%@",aDic[@"data"][@"avgSpeed"]];
        self.carId = [NSString stringWithFormat:@"%@",aDic[@"data"][@"carId"]];
        self.date = [NSString stringWithFormat:@"%@",aDic[@"data"][@"date"]];
        self.maxSpeed = [NSString stringWithFormat:@"%@",aDic[@"data"][@"maxSpeed"]];
        self.mileage = [NSString stringWithFormat:@"%@",aDic[@"data"][@"mileage"]];
        self.minSpeed = [NSString stringWithFormat:@"%@",aDic[@"data"][@"minSpeed"]];
        self.dayId = [NSString stringWithFormat:@"%@",aDic[@"data"][@"id"]];

    }
    return self;
}

- (id)initWithArrAndDic:(NSDictionary *)aDic{
    if (self == [super init]) {
        
        self.avgSpeed = [NSString stringWithFormat:@"%@",aDic[@"avgSpeed"]];
        self.carId = [NSString stringWithFormat:@"%@",aDic[@"carId"]];
        self.date = [NSString stringWithFormat:@"%@",aDic[@"date"]];
        self.maxSpeed = [NSString stringWithFormat:@"%@",aDic[@"maxSpeed"]];
        self.mileage = [NSString stringWithFormat:@"%@",aDic[@"mileage"]];
        self.minSpeed = [NSString stringWithFormat:@"%@",aDic[@"minSpeed"]];
        self.dayId = [NSString stringWithFormat:@"%@",aDic[@"id"]];
        
    }
    return self;
}

@end
