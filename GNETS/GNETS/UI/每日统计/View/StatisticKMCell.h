//
//  StatisticKMCell.h
//  GNETS
//
//  Created by fyc on 16/2/18.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatisticKMCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLa;
@property (weak, nonatomic) IBOutlet UILabel *unitLa;
@property (weak, nonatomic) IBOutlet UIImageView *iconImg;

@property (nonatomic, retain)UIView *backView;

@end
