//
//  StatisticKMCell.m
//  GNETS
//
//  Created by fyc on 16/2/18.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "StatisticKMCell.h"

@implementation StatisticKMCell

- (void)awakeFromNib {
    // Initialization code

    
    self.layer.borderWidth = 0.5;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor
    ;
    
    self.backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kMainScreenWidth/2, 80)];
    self.backView.backgroundColor = [UIColor lightGrayColor];
    self.backView.alpha = 0.1;
    [self addSubview:self.backView];
}

@end
