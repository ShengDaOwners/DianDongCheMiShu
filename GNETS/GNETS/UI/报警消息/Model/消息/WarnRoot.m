//
//  _root_.m
//  消息列表
//
//  Created by _author on 16-03-06.
//  Copyright (c) _companyname. All rights reserved.
//  

/*
	
*/


#import "WarnRoot.h"
#import "DTApiBaseBean.h"
#import "WarnData.h"


@implementation WarnRoot

@synthesize code = _code;
@synthesize data = _data;
@synthesize errmsg = _errmsg;

-(id)initWithDictionary:(NSDictionary*)dict
{
    if (self = [super init])
    {
		DTAPI_DICT_ASSIGN_NUMBER(code, @"0");
		self.data = [DTApiBaseBean arrayForKey:@"data" inDictionary:dict withClass:[WarnData class]];
		DTAPI_DICT_ASSIGN_STRING(errmsg, @"");
    }
    
    return self;
}

-(NSDictionary*)dictionaryValue
{
    NSMutableDictionary *md = [NSMutableDictionary dictionary];
    
	DTAPI_DICT_EXPORT_BASICTYPE(code);
	DTAPI_DICT_EXPORT_ARRAY_BEAN(data);
	DTAPI_DICT_EXPORT_BASICTYPE(errmsg);
    return md;
}
@end
