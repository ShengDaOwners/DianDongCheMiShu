//
//  data.h
//  消息列表
//
//  Created by _author on 16-03-06.
//  Copyright (c) _companyname. All rights reserved.
//

/*
	
*/

#import <Foundation/Foundation.h>
#import "DTApiBaseBean.h"


@interface WarnData : NSObject<NSCoding>
{
	NSNumber *_carId;
	NSString *_createDate;
	NSNumber *_createTime;
	NSNumber *_eventId;
	NSNumber *_eventType;
	NSNumber *_heading;
	NSNumber *_lat;
	NSNumber *_lon;
	NSString *_msg;
	NSNumber *_sourceId;
	NSNumber *_speed;
	NSNumber *_status;
	NSString *_statusDate;
}


@property (nonatomic, copy) NSNumber *carId;
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSNumber *createTime;
@property (nonatomic, copy) NSNumber *eventId;
@property (nonatomic, copy) NSNumber *eventType;
@property (nonatomic, copy) NSNumber *heading;
@property (nonatomic, copy) NSNumber *lat;
@property (nonatomic, copy) NSNumber *lon;
@property (nonatomic, copy) NSString *msg;
@property (nonatomic, copy) NSNumber *sourceId;
@property (nonatomic, copy) NSNumber *speed;
@property (nonatomic, copy) NSNumber *status;
@property (nonatomic, copy) NSString *statusDate;
@property (nonatomic, assign) BOOL isLook;


-(id)initWithDictionary:(NSDictionary*)dict;
-(NSDictionary*)dictionaryValue;
@end
 