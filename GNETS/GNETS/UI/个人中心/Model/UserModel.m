//
//  _root_.m
//  电动车秘书
//
//  Created by _author on 16-02-21.
//  Copyright (c) _companyname. All rights reserved.
//  

/*
	
*/


#import "UserModel.h"
#import "DTApiBaseBean.h"
#import "Userdata.h"


@implementation UserModel

@synthesize code = _code;
@synthesize data = _data;
@synthesize errmsg = _errmsg;


-(id)initWithDictionary:(NSDictionary*)dict
{
    if (self = [super init])
    {
		DTAPI_DICT_ASSIGN_NUMBER(code, @"0");
		self.data = [DTApiBaseBean objectForKey:@"data" inDictionary:dict withClass:[Userdata class]];
		DTAPI_DICT_ASSIGN_STRING(errmsg, @"");
    }
    
    return self;
}

-(NSDictionary*)dictionaryValue
{
    NSMutableDictionary *md = [NSMutableDictionary dictionary];
    
	DTAPI_DICT_EXPORT_BASICTYPE(code);
	DTAPI_DICT_EXPORT_BEAN(data);
	DTAPI_DICT_EXPORT_BASICTYPE(errmsg);
    return md;
}
@end
