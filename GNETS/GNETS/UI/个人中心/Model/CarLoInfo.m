//
//  CarLoInfo.m
//  GNETS
//
//  Created by fyc on 16/2/24.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "CarLoInfo.h"

@implementation CarLoInfo

- (id)initWithaDic:(NSDictionary *)aDic{
    if (self == [super init]) {
        self.code = [NSString stringWithFormat:@"%@",aDic[@"code"]];
        self.errmsg = [NSString stringWithFormat:@"%@",aDic[@"errmsg"]];
        self.acc = [NSString stringWithFormat:@"%@",aDic[@"data"][@"acc"]];
        self.address = [NSString stringWithFormat:@"%@",aDic[@"data"][@"address"]];
        self.carId = [NSString stringWithFormat:@"%@",aDic[@"data"][@"carId"]];
        self.heading = [NSString stringWithFormat:@"%@",aDic[@"data"][@"heading"]];
        self.isOnline = [NSString stringWithFormat:@"%@",aDic[@"data"][@"isOnline"]];
        self.isOpenVf = [NSString stringWithFormat:@"%@",aDic[@"data"][@"isOpenVf"]];
        self.lat = [NSString stringWithFormat:@"%@",aDic[@"data"][@"lat"]];
        self.loc = [NSString stringWithFormat:@"%@",aDic[@"data"][@"loc"]];
        self.lock = [NSString stringWithFormat:@"%@",aDic[@"data"][@"lock"]];
        self.lon = [NSString stringWithFormat:@"%@",aDic[@"data"][@"lon"]];
        self.power = [NSString stringWithFormat:@"%@",aDic[@"data"][@"power"]];
        self.satelliteTime = [NSString stringWithFormat:@"%@",aDic[@"data"][@"satelliteTime"]];
        self.sourceType = [NSString stringWithFormat:@"%@",aDic[@"data"][@"sourceType"]];
        self.speed = [NSString stringWithFormat:@"%@",aDic[@"data"][@"speed"]];

    }
    return self;
}

@end
