//
//  Country.h
//  GNETS
//
//  Created by fyc on 16/3/1.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Country : NSObject

@property (nonatomic, assign)int _id;
@property (nonatomic, assign)int cid;
@property (nonatomic, retain)NSString *name;


@end
