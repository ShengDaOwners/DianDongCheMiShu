//
//  InsuranceInfo.m
//  GNETS
//
//  Created by fyc on 16/2/29.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "InsuranceInfos.h"

@implementation InsuranceInfos

- (id)initWithaDic:(NSDictionary *)aDic{
    if (self == [super init]) {
        self.code = [NSString stringWithFormat:@"%@",aDic[@"code"]];
        self.errmsg = [NSString stringWithFormat:@"%@",aDic[@"errmsg"]];
        self.carId = [NSString stringWithFormat:@"%@",aDic[@"data"][@"carId"]];
        self.city = [NSString stringWithFormat:@"%@",aDic[@"data"][@"city"]];
        self.endDate = [NSString stringWithFormat:@"%@",aDic[@"data"][@"endDate"]];
        self.idNum = [NSString stringWithFormat:@"%@",aDic[@"data"][@"idNum"]];
        self.insurNum = [NSString stringWithFormat:@"%@",aDic[@"data"][@"insurNum"]];
        self.notify = [NSString stringWithFormat:@"%@",aDic[@"data"][@"notify"]];
        self.phone = [NSString stringWithFormat:@"%@",aDic[@"data"][@"phone"]];
        self.province = [NSString stringWithFormat:@"%@",aDic[@"data"][@"province"]];
        self.startDate = [NSString stringWithFormat:@"%@",aDic[@"data"][@"startDate"]];
        self.userName = [NSString stringWithFormat:@"%@",aDic[@"data"][@"userName"]];
        self.startDateStr = [NSString stringWithFormat:@"%@",aDic[@"data"][@"startDateStr"]];
        self.endDateStr = [NSString stringWithFormat:@"%@",aDic[@"data"][@"endDateStr"]];

    }
    return self;
}

@end
