//
//  CarLoInfo.h
//  GNETS
//
//  Created by fyc on 16/2/24.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarLoInfo : NSObject

@property (nonatomic ,retain)NSString *code;
@property (nonatomic ,retain)NSString *errmsg;


@property (nonatomic ,retain)NSString *acc;//0off 1 on
@property (nonatomic ,retain)NSString *address;
@property (nonatomic ,retain)NSString *carId;
@property (nonatomic ,retain)NSString *heading;
@property (nonatomic ,retain)NSString *isOnline;//0offline 1online
@property (nonatomic ,retain)NSString *isOpenVf;
@property (nonatomic ,retain)NSString *lat;
@property (nonatomic ,retain)NSString *loc;
@property (nonatomic ,retain)NSString *lock;//1 locked  0unlocked
@property (nonatomic ,retain)NSString *lon;
@property (nonatomic ,retain)NSString *power;//0 off 1 on
@property (nonatomic ,retain)NSString *satelliteTime;
@property (nonatomic ,retain)NSString *sourceType;
@property (nonatomic ,retain)NSString *speed;

- (id)initWithaDic:(NSDictionary *)aDic;


@end
