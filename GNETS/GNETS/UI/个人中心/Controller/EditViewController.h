//
//  EditViewController.h
//  GNETS
//
//  Created by cqz on 16/4/23.
//  Copyright © 2016年 CQZ. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "BaseViewController.h"
typedef NS_ENUM(NSUInteger, EditType)
{
    BasicType   = 0, //基本资料
    CarDataType = 1, //车辆资料
};

typedef void(^ZDStringBlock)(NSString *);

@interface EditViewController : BaseViewController

@property (nonatomic, copy) NSString *titleStr;
@property (nonatomic, copy) NSString *contentStr;
@property (nonatomic, copy) ZDStringBlock editBlock;
@property (nonatomic, assign) EditType editType;

@property (nonatomic, assign) NSUInteger selectRow;
@end
