//
//  EditViewController.m
//  GNETS
//
//  Created by cqz on 16/4/23.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "EditViewController.h"

@interface EditViewController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UITextField *_editTextF;
    NSString *_parameterStr;
    NSUInteger _selectRow;
}
@property (strong,nonatomic) UITableView *tableView;

@end

@implementation EditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    [self initUI];
}
- (void)initUI
{
    [self setupNaviBarWithTitle:_titleStr];
    [self setupNaviBarWithBtn:NaviLeftBtn title:nil img:@"back_icon"];
    
    _parameterStr = @"";
    if ([_titleStr isEqualToString:@"车辆品牌"]) {
        _parameterStr = @"carBrand";
        
    }else if ([_titleStr isEqualToString:@"车辆型号"]) {
        _parameterStr = @"carModel";

    }else if ([_titleStr isEqualToString:@"电机编号"]) {
        _parameterStr = @"motorNum";

    }else if ([_titleStr isEqualToString:@"车架编号"]) {
        _parameterStr = @"frameNum";

    }else if ([_titleStr isEqualToString:@"车辆颜色"]) {
        _parameterStr = @"carColor";

    }else if ([_titleStr isEqualToString:@"购买日期"]) {
        _parameterStr = @"carDate";

    }else if ([_titleStr isEqualToString:@"购买价格"]) {
        _parameterStr = @"carPrice";

    }else if ([_titleStr isEqualToString:@""]) {
        _parameterStr = @"carPic";
    }else if ([_titleStr isEqualToString:@"姓名"]) {
        _parameterStr = @"userName";
    }else if ([_titleStr isEqualToString:@"性别"]) {
        _parameterStr = @"sex";
    }else if ([_titleStr isEqualToString:@"身份证号"]) {
        _parameterStr = @"idNum";
    }else if ([_titleStr isEqualToString:@"联系电话"]) {
        _parameterStr = @"phone";
    }else if ([_titleStr isEqualToString:@"详细地址"]) {
        _parameterStr = @"address";
    }
    

    if ([_titleStr isEqualToString:@"性别"]) {
        
//        _selectRow = 0;
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 74, kMainScreenWidth, 90) style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.bounces = NO;
        _tableView.backgroundColor = [UIColor clearColor];
        [_tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
        [ self.view addSubview:_tableView];
        [_tableView  registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
  
    }else{
        
        [self setupNaviBarWithBtn:NaviRightBtn title:@"保存" img:nil];
        self.rightBtn.titleLabel.font = Font_16;

        UIView *searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 74, kMainScreenWidth, 40)];
        searchView.layer.masksToBounds = YES;
        searchView.layer.borderWidth = .6f;
        searchView.backgroundColor = [UIColor whiteColor];
        searchView.layer.borderColor = [UIColor colorWithHexString:@"#d4d4d4"].CGColor;
        [self.view addSubview:searchView];
        
        
        _editTextF =[[UITextField alloc] initWithFrame:CGRectMake(10.f, 5, searchView.frame.size.width - 20.f, 30)];
        _editTextF.delegate = self;
        _editTextF.text = _contentStr;
        _editTextF.font = Font_14;
        
        if ([_parameterStr isEqualToString:@"carPrice"]){
            _editTextF.keyboardType = UIKeyboardTypeNumberPad;
        }
        _editTextF.borderStyle = UITextBorderStyleNone;
        _editTextF.returnKeyType = UIReturnKeyDone; //设置按键类型
        [searchView addSubview:_editTextF];
        [_editTextF becomeFirstResponder];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textFieldChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:_editTextF];

    }
    
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(15, 43.5f, kMainScreenWidth-15.f, .5f)];
    lineView.backgroundColor = lineColor;
    [cell.contentView addSubview:lineView];
    
    if (_selectRow == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if (indexPath.row == 0) {
        cell.textLabel.text = @"男";
        lineView.hidden = NO;

    }else{
        cell.textLabel.text = @"女";
        lineView.hidden = YES;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    cell.textLabel.font = Font_15;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView reloadData];
    
    _selectRow = indexPath.row;
    WEAKSELF;
    NSString *sexStr = @"0";
    NSString *trueSex = @"";
    if (indexPath.row == 0) {
        sexStr = @"0";
        trueSex = @"男";
    }else{
        sexStr = @"1";
        trueSex = @"女";
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@",kProjectBaseUrl,UpdateUserInfo];
    NSDictionary *dict  = [NSDictionary dictionaryWithObjectsAndKeys:[[PublicFunction ShareInstance] getAccount].data.carId,@"carId" ,sexStr,_parameterStr,nil];

    [GNETS_NetWorkManger PostJSONWithUrl:url parameters:dict isNeedHead:YES success:^(NSDictionary *jsonDic) {
        
        NSString *str = [NSString stringWithFormat:@"%@",jsonDic[@"code"]];
        if ([str isEqualToString:@"1"]) {
            [JKPromptView showWithImageName:nil message:jsonDic[@"errmsg"]];
            weakSelf.editBlock(trueSex);
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    } fail:^{
    }];

}
#pragma mark-UIButtonEvent
- (void)rightBtnAction
{
    if (_editTextF.text.length == 0) {
        [JKPromptView showWithImageName:nil message:@"请您填写修改内容！"];
        return;
    }

    NSString *url = nil;
    if (_editType == CarDataType) {
        url = [NSString stringWithFormat:@"%@%@",kProjectBaseUrl,UpdateCar];
    }else{
        url = [NSString stringWithFormat:@"%@%@",kProjectBaseUrl,UpdateUserInfo];
    }
    NSString *postModify = _editTextF.text;
    NSDictionary *dict  = [NSDictionary dictionaryWithObjectsAndKeys:[[PublicFunction ShareInstance] getAccount].data.carId,@"carId" ,postModify,_parameterStr,nil];
    WEAKSELF;
    [GNETS_NetWorkManger PostJSONWithUrl:url parameters:dict isNeedHead:YES success:^(NSDictionary *jsonDic) {
        
        NSString *str = [NSString stringWithFormat:@"%@",jsonDic[@"code"]];
        if ([str isEqualToString:@"1"]) {
            [JKPromptView showWithImageName:nil message:jsonDic[@"errmsg"]];
            weakSelf.editBlock(_editTextF.text);
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    } fail:^{
        
    }];
    
    
}
#pragma mark -UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self dismissKeyBoard];
    return true;
}
- (void)textFieldChanged:(NSNotification*)noti{
    
    UITextField *textField = (UITextField *)noti.object;
    
    BOOL flag=[NSString isContainsTwoEmoji:textField.text];
    if (flag){
        textField.text = [NSString disable_emoji:textField.text];
    }
}

#pragma mark - 放下键盘
- (void)dismissKeyBoard{
    [self.view endEditing:YES];
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
