//
//  HandleInsuranceVC.m
//  GNETS
//
//  Created by fyc on 16/2/18.
//  Copyright © 2016年 CQZ. All rights reserved.
//
#define SubmitInsuranseInfo @"insur/uploadInsurInfo.do"
#import "HandleInsuranceVC.h"
#import "InsuranceCell.h"
#import "InsuranceInfoCell.h"
#import "InsuranceRegionCell.h"
#import "FCImagePickerController.h"
#import "DtaePickView.h"
@interface HandleInsuranceVC ()<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UITextFieldDelegate>
{
    NSString *_dateString;
}

@property (nonatomic, strong)NSArray *data;
@property (nonatomic, strong)UITableView *insuranceTable;
@property (nonatomic, strong)NSMutableDictionary *requestDic;
@property (nonatomic, strong)NSMutableArray *tapImages;


@end

@implementation HandleInsuranceVC
- (NSMutableArray *)tapImages{
    if (!_tapImages) {
        _tapImages = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _tapImages;
}
- (NSMutableDictionary *)requestDic{
    if (!_requestDic) {
        _requestDic = [[NSMutableDictionary alloc]initWithCapacity:0];
    }
    return _requestDic;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _dateString = @"必选";
    
    [self setupNaviBarWithTitle:@"办理保险"];
    [self setupNaviBarWithBtn:NaviLeftBtn title:@"我" img:@"back_icon"];
    [self setupNaviBarWithBtn:NaviRightBtn title:@"提交" img:nil];
    
    self.insuranceTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, kMainScreenWidth, kMainScreenHeight - 64) style:UITableViewStyleGrouped];
    self.insuranceTable.delegate = self;
    self.insuranceTable.dataSource = self;
    [self.view addSubview:self.insuranceTable];
    
    NSString *resourcePath = [[NSBundle mainBundle]pathForResource:@"ebike_city" ofType:@"db"];
    
    [self.insuranceTable registerNib:[UINib nibWithNibName:@"InsuranceCell" bundle:nil] forCellReuseIdentifier:@"InsuranceCell"];
    
    [self.insuranceTable registerNib:[UINib nibWithNibName:@"InsuranceInfoCell" bundle:nil] forCellReuseIdentifier:@"InsuranceInfoCell"];
    
    [self.insuranceTable registerNib:[UINib nibWithNibName:@"InsuranceRegionCell" bundle:nil] forCellReuseIdentifier:@"InsuranceRegionCell"];
    
    self.data = @[@[@"车辆照片",@"身份证正面",@"身份证反面",@"合格证照片",@"发票或收据"],@[@"车辆品牌",@"车辆型号",@"购买日期",@"购买价格",@"电机编号",@"车架编号",@"承保区域"]];
    
}
#pragma mark - table
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 5;
    }
    return 7;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        InsuranceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InsuranceCell"];
        cell.titleLa.text = self.data[indexPath.section][indexPath.row];
        
        return cell;
    }else if (indexPath.section == 1&&indexPath.row != 6 &&indexPath.row != 2){
        InsuranceInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InsuranceInfoCell"];
        cell.titleLa.text = self.data[indexPath.section][indexPath.row];
        cell.contentTe.tag = 10000 + indexPath.row;
        cell.contentTe.delegate = self;
        if (indexPath.row == 3) {
            cell.contentTe.keyboardType = UIKeyboardTypeNumberPad;
        }
        
        return cell;
    }else{
        InsuranceRegionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InsuranceRegionCell"];
        cell.titleLa.text = self.data[indexPath.section][indexPath.row];
        if (indexPath.row == 6) {
            cell.regionLa.text = [NSString stringWithFormat:@"%@-%@",[[PublicFunction ShareInstance]getAccount].data.province,[[PublicFunction ShareInstance]getAccount].data.city];
        }
        if(indexPath.row == 2)
        {
            cell.regionLa.text = _dateString;
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
        {
            UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"选取照片" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"相册", nil];
            sheet.tag = 1000 + indexPath.row;
            [sheet showInView:self.view];
            break;
        }
        case 1:
        {
            if (indexPath.row == 2)
           {
               DtaePickView *pickView = [[DtaePickView alloc] init];
               [pickView setDateViewWithTitle:@"选择时间"];
               [pickView showPickView:self];
               pickView.alertBlock = ^(NSString *selectedStr)
               {
                   DLog(@"时间是－－－－%@",selectedStr);
                   _dateString  = selectedStr;
                   InsuranceRegionCell *cell = (InsuranceRegionCell *)[tableView cellForRowAtIndexPath:indexPath];
                   cell.regionLa.text = _dateString;
                   [self.requestDic setObject:_dateString forKey:@"date"];

               };

               
            }
            
            if (indexPath.row == 6)
            {
                
                GPDateView * dateView = [[GPDateView alloc] initWithFrame:CGRectMake(0, kMainScreenHeight-250, kMainScreenWidth, 250) Data:nil];
                
                [dateView showPickerView];
                
                dateView.ActionDistrictViewSelectBlock = ^(NSString *desStr,NSDictionary *selectDistrictDict){
                    InsuranceRegionCell *cell = (InsuranceRegionCell *)[tableView cellForRowAtIndexPath:indexPath];
                    NSString *district;

                    //省市一样  取一个
                    NSRange range = [selectDistrictDict[@"DistrictSelectProvince"] rangeOfString:@"市"];
                    if (range.length > 0) {//包括市
                        cell.regionLa.text = [NSString stringWithFormat:@"%@-%@",selectDistrictDict[@"DistrictSelectProvince"],selectDistrictDict[@"DistrictSelectProvince"]];
                        
                        [self.requestDic setObject:selectDistrictDict[@"DistrictSelectProvince"] forKey:@"provinve"];
                        
                        [self.requestDic setObject:selectDistrictDict[@"DistrictSelectProvince"] forKey:@"city"];

                    }else
                    {
                        cell.regionLa.text = [NSString stringWithFormat:@"%@-%@",selectDistrictDict[@"DistrictSelectProvince"],selectDistrictDict[@"DistrictSelectCity"]];
                        
                        [self.requestDic setObject:selectDistrictDict[@"DistrictSelectProvince"] forKey:@"provinve"];
                        
                        [self.requestDic setObject:selectDistrictDict[@"DistrictSelectCity"] forKey:@"city"];
                    }


                };
                
            }
            break;
        }
        default:
            break;
    }
    
}
//-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
//     if ([touch.view isKindOfClass:[InsuranceCell class]]) {
//         return YES;
//     }
//    return YES;
//}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    FCImagePickerController *picker = [[FCImagePickerController alloc] init];
    picker.delegate = self;
    picker.tag = actionSheet.tag;
    if (buttonIndex == 0) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:nil];

    }else if(buttonIndex == 1){
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:nil];

    }
}
#pragma mark -tapImage
- (void)imgBtnClick:(UITapGestureRecognizer *)tap{
    if (self.tapImages.count == 0) {
        return;
    }
    NSMutableArray *imageViews = [[NSMutableArray alloc]initWithCapacity:0];
    for (UIImage *image in self.tapImages) {
        MJPhoto *photo = [[MJPhoto alloc]init];
        photo.image = image;
        UIImageView *v = [[UIImageView alloc]init];
        v.image = image;
        photo.srcImageView = v;
        [imageViews addObject:photo];
    }
    MJPhotoBrowser *brower = [[MJPhotoBrowser alloc]init];
//    brower.currentPhotoIndex = tap.view.tag - 100;
    brower.photos = imageViews;
    [brower show];
}
#pragma mark - image
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *image = info[@"UIImagePickerControllerOriginalImage"];
    FCImagePickerController *picks = (FCImagePickerController *)picker;
    InsuranceCell *cell  = [self.insuranceTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:picks.tag - 1000 inSection:0]];
    cell.selectLa.text = @"已选中照片";
    
    switch (picks.tag - 1000) {
        case 0:
        {
            UIImage *imageOne =[self handlePicWithImage:image];
            NSData *dataOne = UIImageJPEGRepresentation(imageOne, 0.5);
            [self.requestDic setObject:dataOne forKey:@"picOne"];
            break;
        }
        case 1:
        {
            UIImage *imageTwo =[self handlePicWithImage:image];
            NSData *dataTwo = UIImageJPEGRepresentation(imageTwo, 0.5);
            [self.requestDic setObject:dataTwo forKey:@"picTwo"];

            break;
        }
        case 2:
        {
            UIImage *imageThree =[self handlePicWithImage:image];
            NSData *dataThree = UIImageJPEGRepresentation(imageThree, 0.5);
            [self.requestDic setObject:dataThree forKey:@"picThree"];

            break;
        }
        case 3:
        {
            UIImage *imageFour =[self handlePicWithImage:image];
            NSData *dataFour = UIImageJPEGRepresentation(imageFour, 0.5);
            [self.requestDic setObject:dataFour forKey:@"picFour"];

            break;
        }
        case 4:
        {
            UIImage *imageFive =[self handlePicWithImage:image];
            NSData *dataFive = UIImageJPEGRepresentation(imageFive, 0.5);
            [self.requestDic setObject:dataFive forKey:@"picFive"];

            break;
        }
        default:
            break;
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}
#pragma -mark 压缩图片
- (UIImage *)handlePicWithImage:(UIImage *)image{
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    CGFloat kbSize = imageData.length/1000.0;
    CGFloat scale = 1.0;
    if (kbSize > 100) {
        scale = 100 / kbSize;
    }
    NSData *newData = UIImageJPEGRepresentation(image, scale);
    UIImage *newImage = [UIImage imageWithData:newData];
    
    return newImage;
}
#pragma -mark 提交
- (void)rightBtnAction
{
    if (!self.requestDic[@"picOne"]) {//车辆照片
        [SVProgressHUD showErrorWithStatus:@"请上传车辆照片"];
    }else if (!self.requestDic[@"picTwo"]){
        [SVProgressHUD showErrorWithStatus:@"请上传身份证正面照片"];
    }else if (!self.requestDic[@"picThree"]){
        [SVProgressHUD showErrorWithStatus:@"请上传身份证反面照片"];
    }else if (!self.requestDic[@"picFour"]){
        [SVProgressHUD showErrorWithStatus:@"请上传合格证照片"];
    }else if (!self.requestDic[@"picFive"]){
        [SVProgressHUD showErrorWithStatus:@"请上传发票或收据照片"];
    }else if (!self.requestDic[@"kind"]){
        [SVProgressHUD showErrorWithStatus:@"请填写车辆品牌"];
    }else if (!self.requestDic[@"model"]){
        [SVProgressHUD showErrorWithStatus:@"请填写车辆型号"];
    }else if (!self.requestDic[@"date"]){
        [SVProgressHUD showErrorWithStatus:@"请填写购买日期"];
    }else if (!self.requestDic[@"price"]){
        [SVProgressHUD showErrorWithStatus:@"请填写购买价格"];
    }else if (!self.requestDic[@"electricNo"]){
        [SVProgressHUD showErrorWithStatus:@"请填写电机编号"];
    }else if (!self.requestDic[@"carNo"]){
        [SVProgressHUD showErrorWithStatus:@"请填写车架编号"];
    }
//    else if (!self.requestDic[@"region"]){
//        [SVProgressHUD showErrorWithStatus:@"请选择承保区域"];
//    }
    else{
        //子类继承实现
        if (!self.requestDic[@"province"]) {
            
            [self.requestDic setObject:[[PublicFunction ShareInstance]getAccount].data.province forKey:@"provinve"];
            
            [self.requestDic setObject:[[PublicFunction ShareInstance]getAccount].data.city forKey:@"city"];
        }
        
        [self postRequest];
        
    }

}
- (void)postRequest{
    NSDictionary *para = @{@"carId":[PublicFunction ShareInstance].getAccount.data.carId,@"motorNum":self.requestDic[@"electricNo"],@"frameNum":self.requestDic[@"carNo"],@"carBrand":self.requestDic[@"kind"],@"carModel":self.requestDic[@"model"],@"idPic":self.requestDic[@"picTwo"],@"idbPic":self.requestDic[@"picThree"],@"carPic":self.requestDic[@"picOne"],@"billPic":self.requestDic[@"picFive"],@"cerPic":self.requestDic[@"picFour"],@"carDate":self.requestDic[@"date"],@"carPrice":self.requestDic[@"price"],@"underwritePro":[self.requestDic[@"provinve"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding],@"underwriteCity":[self.requestDic[@"city"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]};
    
    NSDictionary *fileDataDic = @{@"idPic":self.requestDic[@"picTwo"],@"idbPic":self.requestDic[@"picThree"],@"carPic":self.requestDic[@"picOne"],@"billPic":self.requestDic[@"picFive"],@"cerPic":self.requestDic[@"picFour"]};
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kProjectBaseUrl,SubmitInsuranseInfo];
    
    [SVProgressHUD showWithStatus:@"上传资料中"];
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{
//        [GNETS_NetWorkManger setFormDataWithUrl:urlStr WithPara:para WithFileDataDic:fileDataDic WithFileNamePrefix:@"images"  success:^(NSDictionary *dic) {
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                // 更UI
//                if (dic) {
//                    [SVProgressHUD dismiss];
//                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:dic[@"errmsg"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//                    [alert show];
//                }else{
//                    [SVProgressHUD showErrorWithStatus:@"上传失败"];
//                }
//                
//            });
//
//        } fail:^{
//            
//        }];
//        
//    });
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [GNETS_NetWorkManger postUploadWithUrl:urlStr parameters:para WithImgDic:fileDataDic success:^(NSDictionary *jsonDic) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // 更UI
                if (jsonDic) {
                    [SVProgressHUD dismiss];
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:jsonDic[@"errmsg"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alert show];
                }else{
                    [SVProgressHUD showErrorWithStatus:@"上传失败"];
                }
                
            });

        } fail:^{
            
        }];
    });

    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSMutableString *str = [[NSMutableString alloc]initWithString:textField.text];
    [str appendString:string];
    switch (textField.tag - 10000) {
        case 0:
        {
            [self.requestDic setObject:str forKey:@"kind"];
            break;
        }
        case 1:
        {
            [self.requestDic setObject:str forKey:@"model"];

            break;
        }
        case 2:
        {
//            [self.requestDic setObject:str forKey:@"date"];

            break;
        }
        case 3:
        {
            [self.requestDic setObject:str forKey:@"price"];

            break;
        }
        case 4:
        {
            [self.requestDic setObject:str forKey:@"electricNo"];

            break;
        }
        case 5:
        {
            [self.requestDic setObject:str forKey:@"carNo"];

            break;
        }
        default:
            break;
    }
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
