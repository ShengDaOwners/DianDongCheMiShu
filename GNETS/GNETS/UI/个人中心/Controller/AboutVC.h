//
//  AboutVC.h
//  GNETS
//
//  Created by fyc on 16/2/29.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "BaseViewController.h"

@interface AboutVC : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *detailTitleLa;
@property (weak, nonatomic) IBOutlet UILabel *versionLa;
@property (weak, nonatomic) IBOutlet UILabel *companys;
@property (weak, nonatomic) IBOutlet UILabel *creatLa;

@end
