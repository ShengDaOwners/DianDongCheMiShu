//
//  InsuranceInfoVC.m
//  GNETS
//
//  Created by fyc on 16/2/29.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "InsuranceInfoVC.h"
#import "BasicDataCell.h"
#import "InsuranceInfos.h"
@interface InsuranceInfoVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,retain)NSArray *data;
@property (nonatomic,retain)UITableView *insuranceInfoTable;

@end

@implementation InsuranceInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNaviBarWithTitle:@"保险信息"];
    [self setupNaviBarWithBtn:NaviLeftBtn title:@"我" img:@"back_icon"];
    
    self.insuranceInfoTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, kMainScreenWidth, kMainScreenHeight - 64) style:UITableViewStyleGrouped];
    self.insuranceInfoTable.delegate = self;
    self.insuranceInfoTable.dataSource = self;
    [self.view addSubview:self.insuranceInfoTable];
    
    [self.insuranceInfoTable registerNib:[UINib nibWithNibName:@"BasicDataCell" bundle:nil] forCellReuseIdentifier:@"BasicDataCell"];
    
    [self getInsuranceData];
    
    [self getCorrectFormatterWithTime:@"Feb 2, 2016 12:00:00 AM"];
    
}
- (void)getInsuranceData{
    [GNETS_NetWorkManger GetJSONWithUrl:[NSString stringWithFormat:@"%@%@?carId=%@",kProjectBaseUrl,InsuranceInfo,[[PublicFunction ShareInstance]getAccount].data.carId] isNeedHead:YES success:^(NSDictionary *jsonDic) {
        
        InsuranceInfos *insurance = [[InsuranceInfos alloc]initWithaDic:jsonDic];

        self.data = @[@[@{@"title":@"设备编号",@"detail":insurance.carId}],@[@{@"title":@"保险单号",@"detail":insurance.insurNum},@{@"title":@"办理日期",@"detail":insurance.startDateStr},@{@"title":@"起保日期",@"detail":[PublicFunction getCustomTimeFormatWithDateStr:insurance.startDateStr WithFormatter:@"yyyy-MM-dd" WithSchedule:24 * 60 * 60 * 7]},@{@"title":@"截至日期",@"detail":insurance.endDateStr},@{@"title":@"被保险人",@"detail":insurance.userName},@{@"title":@"身份证号",@"detail":insurance.idNum},@{@"title":@"联系电话",@"detail":insurance.phone},@{@"title":@"承包区域",@"detail":[NSString stringWithFormat:@"%@ %@",insurance.province,insurance.city]}]];

        [self.insuranceInfoTable reloadData];
        
    } fail:^{
        
    }];
}
#pragma mark - table
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }
    return 8;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BasicDataCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BasicDataCell"];
    
    cell.titleLa.text = self.data[indexPath.section][indexPath.row][@"title"];
    cell.detailTitleLa.text = self.data[indexPath.section][indexPath.row][@"detail"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSString *)getCorrectFormatterWithTime:(NSString *)time{
    
    NSDate *str = [PublicFunction getDateWithDateStr:@"2013-01-01 12:23:40"];
    
    NSString *month = [time substringToIndex:3];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[time integerValue]];
    NSString *times = [formatter stringFromDate:date];
    return times;
    
}
//根据 时间戳 计算时间
- (NSString *)timeWithStr:(NSString *)timeStr
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timeStr integerValue]];
    NSString *time = [formatter stringFromDate:date];
    return time;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
