//
//  CarDataVC.m
//  GNETS
//
//  Created by fyc on 16/2/23.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "CarDataVC.h"
#import "CarDataCell.h"
#import "CarDataModel.h"
#import "EditViewController.h"
#import "FCImagePickerController.h"
#import "RegisterWindow.h"
#import "DtaePickView.h"

@interface CarDataVC ()<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UITextFieldDelegate>
{
    UIImage *_headImg;//图片
    NSString *_priceStr;
}

@property (nonatomic,strong)NSMutableArray *data;
@property (nonatomic,strong)UITableView *selfDataTable;
@property (nonatomic,strong) NSMutableDictionary *requestDic;
@property (nonatomic, copy)  NSString *showStr;//远程控制
@property (nonatomic,copy) NSString *voltage;//电压

@end

@implementation CarDataVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.requestDic = [[NSMutableDictionary alloc] init];
    
    self.navigationController.navigationBarHidden = YES;
    [self setupNaviBarWithTitle:@"车辆资料"];
    [self setupNaviBarWithBtn:NaviLeftBtn title:_leftTitle img:@"back_icon"];
//    [self setupNaviBarWithBtn:NaviRightBtn title:@"车辆资料" img:nil];
    
    self.data = [NSMutableArray array];
    self.selfDataTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, kMainScreenWidth, kMainScreenHeight - 64) style:UITableViewStylePlain];
    self.selfDataTable.delegate = self;
    self.selfDataTable.dataSource = self;
    [self.selfDataTable setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    self.selfDataTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.selfDataTable];
    
    [self.selfDataTable registerNib:[UINib nibWithNibName:@"CarDataCell" bundle:nil] forCellReuseIdentifier:@"CarDataCell"];
    
    [self getRequest];
    
}
- (void)getRequest{
    WEAKSELF;
    [GNETS_NetWorkManger GetJSONWithUrl:[NSString stringWithFormat:@"%@%@?carId=%@",kProjectBaseUrl,CARINFO,[[PublicFunction ShareInstance] getAccount].data.carId] isNeedHead:YES success:^(NSDictionary *jsonDic) {
        
        CarDataModel *carModel = [[CarDataModel alloc]initWithaDic:jsonDic];
        _priceStr = carModel.carPrice;
        weakSelf.showStr = nil;
        NSString *voltageStr = carModel.voltage;
        if ([voltageStr isEqualToString:@"(null)"]) {
            voltageStr = @"";
        }

        NSString *controStr = carModel.controlType;
        if ([controStr isEqualToString:@"(null)"]) {
            controStr = @"";
        }


        weakSelf.data = [NSMutableArray arrayWithObjects:@{@"title":@"车辆品牌",@"detail":carModel.carBrand},@{@"title":@"车辆型号",@"detail":carModel.carModel},@{@"title":@"电机编号",@"detail":carModel.motorNum},@{@"title":@"车架编号",@"detail":carModel.frameNum},@{@"title":@"远程控制",@"detail":[NSString stringWithFormat:@"%@",controStr]},@{@"title":@"电动车电压",@"detail":[NSString stringWithFormat:@"%@",voltageStr]},@{@"title":@"购买日期",@"detail":[NSString stringWithFormat:@"%@",carModel.carDate]},@{@"title":@"购买价格",@"detail":[NSString stringWithFormat:@"¥%@",carModel.carPrice]},@{@"title":@"车辆照片",@"detail":@""},@{@"title":@"",@"detail":carModel.carPic}, nil];
        
        [PublicFunction showErrorMsg:jsonDic];
        [weakSelf.selfDataTable reloadData];
        
    } fail:^{
        
    }];
}
#pragma mark - table 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 9) {
        return 150;
    }
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.data.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CarDataCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CarDataCell"];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if (indexPath.row == 8) {
        cell.picImgView.hidden = NO;
        cell.detailLa.hidden = YES;
    }else{
        cell.picImgView.hidden = YES;
        cell.detailLa.hidden = NO;
    }


    
    if (indexPath.row == 9) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        for (UIView *v in cell.contentView.subviews) {
            [v removeFromSuperview];
        }
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        
        UIImageView *picImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kMainScreenWidth, 150)];
//        if ([self.data[indexPath.row][@"detail"] isEqualToString:@""]) {
//            
//        }
        if (_headImg) {
            picImg.image = _headImg;
        }else{
            [picImg sd_setImageWithURL:[NSURL URLWithString:self.data[indexPath.row][@"detail"]] placeholderImage:[UIImage imageNamed:@"icon_default_ebike"]];
        }
        picImg.contentMode = UIViewContentModeCenter;
        picImg.clipsToBounds = YES;
        picImg.tag = 1000 + indexPath.row;
        [cell.contentView addSubview:picImg];
        return cell;
    }
    if (self.data.count >0) {
        cell.titleLa.text = self.data[indexPath.row][@"title"];
        if (indexPath.row == 4) {

            if (_showStr.length == 0) {
                NSString *controlStr = self.data[indexPath.row][@"detail"];
                if ([controlStr isEqualToString:@"1"]) {
                    _showStr = @"远程锁车";
                }else if ([controlStr isEqualToString:@"2"]){
                    _showStr = @"语音寻车";
                }else if ([controlStr isEqualToString:@"3"]){
                    _showStr = @"一键启动";
                }else{
                    _showStr = @"";
                }
            }
            
            cell.detailLa.text = _showStr;

        }else if (indexPath.row == 5){
            NSString *str = self.data[indexPath.row][@"detail"];
            if ([str isEqualToString:@"(null)"]) {
                str = @"";
            }
            cell.detailLa.text = str;

        }else{
            cell.detailLa.text = self.data[indexPath.row][@"detail"];
        }
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    WEAKSELF;
    if (indexPath.row == 6) {
        
        DtaePickView *pickView = [[DtaePickView alloc] init];
        [pickView setDateViewWithTitle:@"选择时间"];
        [pickView showPickView:self];
        pickView.alertBlock = ^(NSString *selectedStr)
        {
            DLog(@"时间是－－－－%@",selectedStr);
            
            NSDictionary *dict  = [NSDictionary dictionaryWithObjectsAndKeys:[[PublicFunction ShareInstance] getAccount].data.carId,@"carId" ,selectedStr,@"carDate",nil];
            WEAKSELF;
            [GNETS_NetWorkManger PostJSONWithUrl:[NSString stringWithFormat:@"%@%@",kProjectBaseUrl,UpdateCar] parameters:dict isNeedHead:YES success:^(NSDictionary *jsonDic) {
                
                NSString *str = [NSString stringWithFormat:@"%@",jsonDic[@"code"]];
                if ([str isEqualToString:@"1"]) {
                    [JKPromptView showWithImageName:nil message:jsonDic[@"errmsg"]];
                    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:weakSelf.data[indexPath.row][@"title"],@"title",selectedStr,@"detail", nil];
                    [weakSelf.data replaceObjectAtIndex:indexPath.row withObject:dic];
                    [tableView  reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section], nil] withRowAnimation:UITableViewRowAnimationNone];
                    
                }
            } fail:^{
                
            }];

            
        };

        
    }else if (indexPath.row <8 && indexPath.row != 6){
        if (indexPath.row == 4) {
            
            RegisterWindow * window = [[RegisterWindow alloc] initWithFrame:kMainScreenFrameRect withType:remoteControl withString:_showStr];
            window.strBlock = ^(NSString *str){
                
                NSString *controlStr = @"1";
                if ([str isEqualToString:@"远程锁车"]) {
                    controlStr = @"1";
                }else if ([str isEqualToString:@"语音寻车"]){
                    controlStr = @"2";
                }else {
                    controlStr = @"3";
                }
                
                
                [weakSelf modificationCarData:@"controlType" withContent:controlStr RequestSuccess:^{
                    
                    weakSelf.showStr = str;
                    [weakSelf.selfDataTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:4 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }];
            };
            [self.view addSubview:window];
            
            
        }else if (indexPath.row == 5){
            
            _voltage = self.data[indexPath.row][@"detail"];
            RegisterWindow * window = [[RegisterWindow alloc] initWithFrame:kMainScreenFrameRect withType:TheElectricVoltage withString:_voltage];
            window.strBlock = ^(NSString *str){
                
                [weakSelf modificationCarData:@"voltage" withContent:str RequestSuccess:^{
                    
                    weakSelf.voltage = str;
                    [weakSelf.data replaceObjectAtIndex:5 withObject:@{@"title":@"电动车电压",@"detail":[NSString stringWithFormat:@"%@",str]}];
                    [weakSelf.selfDataTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:5 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    
                }];
            };
            [self.view addSubview:window];
            
            
        }else{
            EditViewController *vc = [EditViewController new];
            vc.titleStr = self.data[indexPath.row][@"title"];
            vc.editType = CarDataType;
            if (indexPath.row == 7) {
                vc.contentStr = _priceStr;
            }else{
                vc.contentStr = self.data[indexPath.row][@"detail"];
            }
            vc.editBlock = ^(NSString *str){
                
                if (indexPath.row == 7) {
                    _priceStr = str;
                }
                
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:weakSelf.data[indexPath.row][@"title"],@"title",str,@"detail", nil];
                [weakSelf.data replaceObjectAtIndex:indexPath.row withObject:dic];
                [tableView  reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section], nil] withRowAnimation:UITableViewRowAnimationNone];
                
            };
            [self.navigationController pushViewController:vc animated:YES];
            
        }

    }else if (indexPath.row == 8){
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"选取照片" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"相册", nil];
        sheet.tag = 1000 + indexPath.row;
        [sheet showInView:self.view];
    }else if (indexPath.row == 9) {
        UIImageView *carImg = [cell viewWithTag:1000 + indexPath.row];
        MJPhoto *photo = [[MJPhoto alloc]init];
        photo.image = carImg.image;
        photo.srcImageView = carImg;
        MJPhotoBrowser *broswer = [[MJPhotoBrowser alloc]init];
        broswer.currentPhotoIndex = 0;
        broswer.photos = @[photo];
        [broswer show];
    }

}
#pragma mark -选取照片
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    FCImagePickerController *picker = [[FCImagePickerController alloc] init];
    picker.delegate = self;
    picker.tag = actionSheet.tag;
    if (buttonIndex == 0) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }else if(buttonIndex == 1){
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
    }
}
#pragma mark - image
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *image = info[@"UIImagePickerControllerOriginalImage"];
//    FCImagePickerController *picks = (FCImagePickerController *)picker;
    _headImg = image;
    UIImage *imageOne =[self handlePicWithImage:image];
    NSData *dataOne = UIImageJPEGRepresentation(imageOne, 0.5);
    
    NSDictionary *dict1  = [NSDictionary dictionaryWithObjectsAndKeys:[[PublicFunction ShareInstance] getAccount].data.carId,@"carId" ,nil];

    NSDictionary *dict2 = [NSDictionary dictionaryWithObjectsAndKeys:dataOne, @"carPic",nil];
    
    WEAKSELF;
    [picker dismissViewControllerAnimated:YES completion:^{
        
        [SVProgressHUD showWithStatus:@"正在提交..."];

        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            
            [GNETS_NetWorkManger postUploadWithUrl:[NSString stringWithFormat:@"%@%@",kProjectBaseUrl,UpdateCar]  parameters:dict1 WithImgDic:dict2 success:^(NSDictionary *jsonDic) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // 更UI
                    if (jsonDic) {
                        [SVProgressHUD dismiss];
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:jsonDic[@"errmsg"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                        [alert show];
                        [weakSelf.selfDataTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:9 inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
                    }else{
                        [SVProgressHUD showErrorWithStatus:@"上传失败"];
                    }
                    
                });
                
            } fail:^{
                [SVProgressHUD dismiss];
            }];
        });

        
    }];
}
#pragma mark -修改数据
- (void)modificationCarData:(NSString *)para
                withContent:(NSString *)content
             RequestSuccess:(void (^)())success;
{

    NSDictionary *dict  = [NSDictionary dictionaryWithObjectsAndKeys:[[PublicFunction ShareInstance] getAccount].data.carId,@"carId" ,content,para,nil];
    [GNETS_NetWorkManger PostJSONWithUrl:[NSString stringWithFormat:@"%@%@",kProjectBaseUrl,UpdateCar] parameters:dict isNeedHead:YES success:^(NSDictionary *jsonDic) {
        
        NSString *str = [NSString stringWithFormat:@"%@",jsonDic[@"code"]];
        if ([str isEqualToString:@"1"]) {
            [JKPromptView showWithImageName:nil message:jsonDic[@"errmsg"]];
            success();
        }
    } fail:^{
        
    }];

}
#pragma -mark 压缩图片
- (UIImage *)handlePicWithImage:(UIImage *)image{
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    CGFloat kbSize = imageData.length/1000.0;
    CGFloat scale = 1.0;
    if (kbSize > 100) {
        scale = 100 / kbSize;
    }
    NSData *newData = UIImageJPEGRepresentation(image, scale);
    UIImage *newImage = [UIImage imageWithData:newData];
    return newImage;
}

//- (void)leftBtnAction
//{
//    [self dismissViewControllerAnimated:YES completion:nil];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
