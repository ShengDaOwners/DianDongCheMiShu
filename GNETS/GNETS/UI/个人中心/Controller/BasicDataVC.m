//
//  BasicDataVC.m
//  GNETS
//
//  Created by fyc on 16/2/23.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "BasicDataVC.h"
#import "BasicDataCell.h"
#import "EditViewController.h"
#import "MyPickView.h"

@interface BasicDataVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) NSMutableArray *data;
@property (nonatomic,retain) UITableView *selfDataTable;

@end

@implementation BasicDataVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupNaviBarWithTitle:@"基本资料"];
    [self setupNaviBarWithBtn:NaviLeftBtn title:@"我" img:@"back_icon"];
    
    self.selfDataTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, kMainScreenWidth, kMainScreenHeight - 64) style:UITableViewStyleGrouped];
    self.selfDataTable.delegate = self;
    self.selfDataTable.dataSource = self;
    self.selfDataTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.selfDataTable];
    
    [self.selfDataTable registerNib:[UINib nibWithNibName:@"BasicDataCell" bundle:nil] forCellReuseIdentifier:@"BasicDataCell"];
    
    [self getUserBasicData];
    
}
- (void)getUserBasicData{
    [GNETS_NetWorkManger GetJSONWithUrl:[NSString stringWithFormat:@"%@%@?carId=%@",kProjectBaseUrl,INFOURL,[[PublicFunction ShareInstance]getAccount].data.carId] isNeedHead:YES success:^(NSDictionary *jsonDic) {
        //更新用户数据
        [[PublicFunction ShareInstance]loginSuccessWithAccount:jsonDic];
        
        UserModel *user = [[UserModel alloc]initWithDictionary:jsonDic];
        
        NSString *sex = user.data.sex.intValue == 0?@"男":@"女";
        
        self.data = [NSMutableArray array];
        NSMutableArray *arr3 = [NSMutableArray arrayWithObjects:@{@"title":@"姓名",@"detail":user.data.userName},@{@"title":@"性别",@"detail":sex},@{@"title":@"身份证号",@"detail":user.data.idNum},@{@"title":@"联系电话",@"detail":user.data.phone},@{@"title":@"所在地区",@"detail":[NSString stringWithFormat:@"%@-%@-%@",user.data.province,user.data.city,user.data.area]},@{@"title":@"详细地址",@"detail":user.data.address}, nil];
        self.data = [NSMutableArray arrayWithObjects:@[@{@"title":@"设备编号",@"detail":[NSString stringWithFormat:@"%@",user.data.carId]}],@[@{@"title":@"IMEI",@"detail":[NSString stringWithFormat:@"%@",user.data.imei]},@{@"title":@"数据卡号",@"detail":user.data.telNum},@{@"title":@"开通时间",@"detail":user.data.activeDate},@{@"title":@"到期时间",@"detail":user.data.expireDate}],arr3, nil];
        
        [self.selfDataTable reloadData];
        
    } fail:^{
        
    }];
}
#pragma mark - table
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return 4;
    }
    return 6;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BasicDataCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BasicDataCell"];
    
    if (indexPath.section == 2) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.titleLa.text = self.data[indexPath.section][indexPath.row][@"title"];
    cell.detailTitleLa.text = self.data[indexPath.section][indexPath.row][@"detail"];
    
    if ([cell.titleLa.text isEqualToString:@"设备编号"]) {
        cell.lineView.hidden = YES;
    }else if ([cell.titleLa.text isEqualToString:@"到期时间"]){
        cell.lineView.hidden = YES;
    }else if ([cell.titleLa.text isEqualToString:@"详细地址"]){
        cell.lineView.hidden = YES;
    }else{
        cell.lineView.hidden = NO;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WEAKSELF;
    if (indexPath.section == 2) {
        
        if (indexPath.row == 4) {
            MyPickView *pickView = [[MyPickView alloc] initWithFrame:CGRectMake(0, 0, kMainScreenWidth, kMainScreenHeight)];
            pickView.pickBlock = ^(NSString *proviceStr,NSString *cityStr,NSString *districtlistStr){
                DLog(@"%@－%@－%@",proviceStr,cityStr,districtlistStr);
                NSString *pcdStr = [NSString stringWithFormat:@"%@-%@-%@",proviceStr,cityStr,districtlistStr];
                NSMutableArray *arr3 = self.data[2];
                [arr3 replaceObjectAtIndex:4 withObject:@{@"title":@"详细地址",@"detail":pcdStr}];
                [tableView  reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section], nil] withRowAnimation:UITableViewRowAnimationNone];
            };
            [self.view addSubview:pickView];

        }else{
            
            EditViewController *vc = [EditViewController new];
            vc.titleStr = self.data[2][indexPath.row][@"title"];
            vc.editType = BasicType;
            if (indexPath.row == 1) {
                NSString *sexIndex = self.data[2][1][@"detail"];
                vc.selectRow = [sexIndex isEqualToString:@"男"]?0:1;
            }

            vc.contentStr = self.data[2][indexPath.row][@"detail"];

            vc.editBlock = ^(NSString *str){
                NSMutableArray *arr3 = weakSelf.data[2];
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:arr3[indexPath.row][@"title"],@"title",str,@"detail", nil];
                [arr3 replaceObjectAtIndex:indexPath.row withObject:dic];
                [tableView  reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section], nil] withRowAnimation:UITableViewRowAnimationNone];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }

    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
