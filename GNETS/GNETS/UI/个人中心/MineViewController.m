//
//  MineViewController.m
//  GNETS
//
//  Created by tcnj on 16/2/16.
//  Copyright © 2016年 CQZ. All rights reserved.
//
#import "ViewController.h"
#import "MineViewController.h"
#import "MineInfoCell.h"
#import "MineDataCell.h"
#import "HandleInsuranceVC.h"
#import "BasicDataVC.h"
#import "CarDataVC.h"
#import "CarLoInfo.h"
#import "ClauseVC.h"
#import "InsuranceInfoVC.h"
#import "AboutVC.h"
@interface MineViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,retain)NSArray *data;
@property (nonatomic, retain)UITableView *mineTable;
@property (nonatomic, retain)CarLoInfo *carInfo;
@property (nonatomic, assign)NSUInteger lock;

@end

@implementation MineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupNaviBarWithTitle:@"我"];
    
    self.mineTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, kMainScreenWidth, kMainScreenHeight - 64 - 49) style:UITableViewStyleGrouped];
    self.mineTable.delegate = self;
    self.mineTable.dataSource = self;
    [self.view addSubview:self.mineTable];
    self.mineTable.separatorStyle = UITableViewCellSeparatorStyleNone;

    [self.mineTable registerNib:[UINib nibWithNibName:@"MineDataCell" bundle:nil] forCellReuseIdentifier:@"dataCell"];
    
    [self.mineTable registerNib:[UINib nibWithNibName:@"MineInfoCell" bundle:nil] forCellReuseIdentifier:@"infoCell"];
    
    self.data = @[@[@{@"icon":@"user_info_icon",@"title":@"基本资料"}],@[@{@"icon":@"user_info_icon",@"title":@"基本资料"},@{@"icon":@"car_info_icon",@"title":@"车辆资料"}],@[@{@"icon":@"insur_icon",@"title":@"办理保险"},@{@"icon":@"insur_item_icon",@"title":@"保险条款"}],@[@{@"icon":@"icon_service_terms",@"title":@"服务条款"},@{@"icon":@"icon_contact_service",@"title":@"联系客服"},@{@"icon":@"icon_about",@"title":@"关于"}]];
    
}
- (void)viewWillAppear:(BOOL)animated{
    
//    [self getUserBasicData];

    
    [super viewWillAppear:animated];
}
#pragma mark - table
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 83;
    }
    return 38;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return 2;
    }else if (section == 2){
        return 2;
    }else if (section == 3){
        return 3;
    }
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 14;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        MineDataCell *cell = [tableView dequeueReusableCellWithIdentifier:@"dataCell"];
        cell.nameLa.text = [[PublicFunction ShareInstance] getAccount].data.userName;
        cell.DeviceNo.text = [NSString stringWithFormat:@"%@",[[PublicFunction ShareInstance]getAccount].data.carId];
        cell.headImg.image = [UIImage imageNamed:@"gaksee_logo"];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 82.5, kMainScreenWidth , .5)];
        lineView.backgroundColor = RGBACOLOR(235, 235, 236, 1);
        [cell.contentView addSubview:lineView];
        return cell;
    }else if (indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3){
        MineInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"infoCell"];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 37.5, kMainScreenWidth , .5)];
        lineView.backgroundColor = RGBACOLOR(235, 235, 236, 1);
        [cell.contentView addSubview:lineView];

        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (indexPath.section == 1 &&indexPath.row == 2) {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        NSArray *datas = [self.data objectAtIndex:indexPath.section];
        NSDictionary *info = datas[indexPath.row];
        cell.titleLa.text = info[@"title"];
        cell.headImg.image = [UIImage imageNamed:info[@"icon"]];
        
        return cell;
    }else{
        [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 37.5, kMainScreenWidth , .5)];
        lineView.backgroundColor = RGBACOLOR(235, 235, 236, 1);
        [cell.contentView addSubview:lineView];

        UILabel *exit = [[UILabel alloc]init];
        exit.center = CGPointMake(kMainScreenWidth/2, 38/2);
        exit.bounds = CGRectMake(0, 0, 100, 20);
        exit.text = @"退出登录";
        exit.textAlignment = NSTextAlignmentCenter;
        exit.font = [UIFont systemFontOfSize:15.0];
        [cell.contentView addSubview:exit];
        
        
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
        {

            break;
        }
        case 1:
        {
            if (indexPath.row == 0) {//基本资料
                BasicDataVC *vc = [[BasicDataVC alloc]init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }else if (indexPath.row == 1){//车辆资料
                CarDataVC *vc = [[CarDataVC alloc]init];
                vc.leftTitle = @"我";
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
            break;
        }
        case 2:
        {
            if (indexPath.row == 0) {//办理保险
                NSString *insurUpdateTime = [[PublicFunction ShareInstance]getAccount].data.insurUpdateTime;
                NSString *insurNum = [[PublicFunction ShareInstance]getAccount].data.insurNum;
                if (![insurNum isEqualToString:@""]) {//保险信息
                    InsuranceInfoVC *vc = [[InsuranceInfoVC alloc]init];
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                }
                else if (![insurUpdateTime isEqualToString:@""]) {
                    [SVProgressHUD showErrorWithStatus:@"您的资料已提交，无需再次操作"];
                }
                else{
                    HandleInsuranceVC *vc = [[HandleInsuranceVC alloc]init];
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }else{//保险条款
                ClauseVC *vc = [[ClauseVC alloc]init];
                vc.urlString = @"http://api.gnets.cn/app/h5/insurance_policy.html";
                vc.claTitle = @"保险条款";
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
            break;
        }
        case 3:
        {
            if (indexPath.row == 0) {//服务条款
                
                ClauseVC *vc = [[ClauseVC alloc]init];
                vc.hidesBottomBarWhenPushed = YES;
                vc.claTitle = @"服务条款";
                vc.urlString = @"http://api.gnets.cn/app/h5/service_terms.html";
                [self.navigationController pushViewController:vc animated:YES];
                
            }else if (indexPath.row == 1){//打电话
                
                
                NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"0531-67805000"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
                
            }else{
                AboutVC *vc = [[AboutVC alloc]init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
            break;
        }
        case 4:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您确定要退出登录吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.delegate = self;
            [alert show];


            break;
        }
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        //退出登录

            [SVProgressHUD showWithStatus:@"正在退出登录.."];
            [GNETS_NetWorkManger GetJSONWithUrl:[NSString stringWithFormat:@"%@%@?carId=%@",kProjectBaseUrl,Logout,[[PublicFunction ShareInstance]getAccount].data.carId] isNeedHead:YES success:^(NSDictionary *jsonDic) {
                [SVProgressHUD dismiss];
                if ([jsonDic[@"errmsg"] isEqualToString:@"ok"]) {
                    [USER_D removeObjectForKey:@"user_phone"];
                    [USER_D removeObjectForKey:@"user_password"];
                    [USER_D removeObjectForKey:@"WARNMSG"];
                    [USER_D removeObjectForKey:@"UNREAD"];
                    [USER_D synchronize];
                    [PublicFunction ShareInstance].m_user.data.province = nil;
                    ViewController *v = [[ViewController alloc]init];
                    [v isAutoLogin];
                    
                }else if ([jsonDic[@"code"] isEqualToString:@"0"]){
                    [SVProgressHUD showErrorWithStatus:jsonDic[@"errmsg"]];
                }
                
            } fail:^{
                
            }];
        }
}
- (void)exitLogin:(id)sender{
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
