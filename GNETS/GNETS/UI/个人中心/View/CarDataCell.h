//
//  CarDataCell.h
//  GNETS
//
//  Created by fyc on 16/2/23.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarDataCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLa;
@property (strong, nonatomic)  UILabel *detailLa;
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UIImageView *picImgView;

@end
