//
//  RegisterWindow.h
//  GNETS
//
//  Created by cqz on 16/6/11.
//  Copyright © 2016年 CQZ. All rights reserved.
//
#import <UIKit/UIKit.h>
typedef  NS_ENUM(NSUInteger,RegisterWindowType){
    TheElectricVoltage = 0,//电动车电压
    remoteControl = 1,//远程控制
};

typedef void(^RStringBlock)(NSString *string);

@interface RegisterWindow : UIWindow

@property (nonatomic ,strong) UIView *zd_superView;
@property (nonatomic, copy) RStringBlock strBlock;
@property (nonatomic, assign) RegisterWindowType type;
-(void)zd_Windowclose;

- (id)initWithFrame:(CGRect)frame withType:(RegisterWindowType)type withString:(NSString *)contentStr;
@end
