//
//  InsuranceCell.h
//  GNETS
//
//  Created by fyc on 16/2/18.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsuranceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLa;
//@property (weak, nonatomic) IBOutlet UIImageView *uploadPic;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *selectLa;
@property (nonatomic, strong)NSIndexPath *indexPath;
@end
