//
//  MineDataCell.h
//  GNETS
//
//  Created by fyc on 16/2/18.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineDataCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *DeviceNo;
@property (weak, nonatomic) IBOutlet UILabel *nameLa;
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@end
