//
//  RegisterWindow.m
//  GNETS
//
//  Created by cqz on 16/6/11.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "RegisterWindow.h"
#import "UIView+Tap.h"
#define zd_width [UIScreen mainScreen].bounds.size.width
#define zd_height [UIScreen mainScreen].bounds.size.height

static NSString *const NavCellIdentifier = @"NavCellIdentifier";

@interface RegisterWindow()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    
   
}

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArrays;
@property (copy, nonatomic) NSString *contentStr;
@property (assign, nonatomic)  NSUInteger selectIndex;
@property (nonatomic,strong) UITextField *vTextF;

@end
@implementation RegisterWindow
- (id)initWithFrame:(CGRect)frame withType:(RegisterWindowType)type withString:(NSString *)contentStr{
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        self.windowLevel = UIWindowLevelAlert;
        
        _type = type;
        _contentStr = contentStr;
        _selectIndex = -1;
        
        if (_type == TheElectricVoltage) {
            self.zd_superView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, zd_width-60, 250)];

        }else{
            self.zd_superView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, zd_width-60, 210)];

        }
        self.zd_superView.backgroundColor = [UIColor whiteColor];
        self.zd_superView.center = CGPointMake(zd_width/2.0,0);
        [UIView animateWithDuration:1 delay:0.0 usingSpringWithDamping:0.5 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
            self.zd_superView.center = CGPointMake(zd_width/2.0,zd_height/2.0);
        } completion:^(BOOL finished) {
        }];
        self.zd_superView.layer.borderWidth = 1.f;
        self.zd_superView.layer.borderColor = [UIColor clearColor].CGColor;
        self.zd_superView.layer.cornerRadius = 5.f;
        self.zd_superView.clipsToBounds = YES;
        [self addSubview:self.zd_superView];
        
        [self initUI];
        [self makeKeyAndVisible];
    }
    return self;
}
#pragma mark -布局界面
- (void)initUI
{WEAKSELF;
    float width = zd_width-60;

    UILabel *headlab = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, zd_width-90, 20)];
    if (_type == TheElectricVoltage) {
        _dataArrays = [NSMutableArray arrayWithObjects:@"48V",@"60V",@"72V", nil];
        _vTextF =[[UITextField alloc] initWithFrame:CGRectMake(15.f, 175.f, width - 30, 30)];
        [self.zd_superView addSubview:_vTextF];
        _vTextF.placeholder = @"请输入其他规格电压";
        _vTextF.delegate = self;
        _vTextF.borderStyle = UITextBorderStyleNone;
        _vTextF.returnKeyType = UIReturnKeyDone; //设置按键类型
        _vTextF.keyboardType = UIKeyboardTypeNumberPad;
        _vTextF.font = Font_14;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textFieldChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:_vTextF];

        __block NSString *compareStr = [NSString stringWithFormat:@"%@V",_contentStr];
        [_dataArrays enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            if ([compareStr isEqualToString:obj]) {
                weakSelf.selectIndex = idx;
            }
        }];
        
        if (_selectIndex == -1) {
            _vTextF.text = _contentStr;
        }
        
        headlab.text = @"选择电动车电压";
    }else{
        _dataArrays = [NSMutableArray arrayWithObjects:@"远程锁车",@"语音寻车",@"一键启动", nil];
        [_dataArrays enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            if ([weakSelf.contentStr isEqualToString:obj]) {
                weakSelf.selectIndex = idx;
            }
        }];
        headlab.text = @"选择远程控制功能";
    }

    headlab.textAlignment = NSTextAlignmentCenter;
//    headlab.font = Font_18;
//    headlab.textColor = [UIColor whiteColor];
    [self.zd_superView addSubview:headlab];
    
    
    UIView *lineview1 = [[UIView alloc] initWithFrame:CGRectMake(0, 49.4f, width, .6f)];
    lineview1.backgroundColor = lineColor;
    [self.zd_superView addSubview:lineview1];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,Orgin_y(lineview1), width, 120) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.bounces = NO;
    [_tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [ self.zd_superView addSubview:_tableView];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NavCellIdentifier];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    

    
    float btnY = 0.f;
    if (_type == TheElectricVoltage) {
        
        UIView *lineview1 = [[UIView alloc] initWithFrame:CGRectMake(0, Orgin_y(_vTextF) +4.4f, width, .6f)];
        lineview1.backgroundColor = lineColor;
        [self.zd_superView addSubview:lineview1];

        btnY = Orgin_y(lineview1);

    }else{
        btnY = Orgin_y(_tableView);
    }
    
    //按钮
    
    
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.titleLabel.font = Font_15;
    cancelBtn.tag = 3001;
    cancelBtn.backgroundColor = [UIColor whiteColor];
    cancelBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [cancelBtn setTitleColor:RGBACOLOR(49, 120, 250, 1) forState:0];
    cancelBtn.frame = CGRectMake(0, btnY, (width - .6f)/2.f , 40);
    [cancelBtn setTitle:@"取消" forState:0];
    //    navBtn.layer.masksToBounds = YES;
    //    navBtn.layer.cornerRadius = 5.f;
    [cancelBtn addTarget:self action:@selector(cancelOrSureEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.zd_superView addSubview:cancelBtn];
    
    UIView *lineView3 = [[UIView alloc] initWithFrame:CGRectMake(Orgin_x(cancelBtn), btnY, .6f, 40)];
    lineView3.backgroundColor = RGBACOLOR(219, 219, 223, 1);
    [self.zd_superView addSubview:lineView3];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.backgroundColor = [UIColor whiteColor];
    sureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [sureBtn setTitleColor:RGBACOLOR(49, 120, 250, 1) forState:0];
    sureBtn.tag = 3002;
    sureBtn.frame = CGRectMake(Orgin_x(lineView3), btnY, (width-.6f)/2.f , 40);
    [sureBtn setTitle:@"确定" forState:0];
    //    navBtn.layer.masksToBounds = YES;
    //    navBtn.layer.cornerRadius = 5.f;
    [sureBtn addTarget:self action:@selector(cancelOrSureEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.zd_superView addSubview:sureBtn];
    
    [self.zd_superView whencancelsToucheTapped:^{
        
    }];
    
    [self whencancelsToucheTapped:^{
        
        [self dismissKeyBoard];
    }];
    
    
}
#pragma mark -UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArrays count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:NavCellIdentifier];
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (_dataArrays.count >0){
        cell.textLabel.text = _dataArrays[indexPath.row];
    }
    if (indexPath.row == _selectIndex) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.textLabel.font = Font_15;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissKeyBoard];
    if (_type == TheElectricVoltage) {
        _vTextF.text = @"";
        if (indexPath.row == 0) {
            _contentStr = @"48";
        }else if (indexPath.row == 1){
            _contentStr = @"60";
        }else{
            _contentStr = @"72";
        }
        
    }else{
        _contentStr = _dataArrays[indexPath.row];
    }
    
    _selectIndex = indexPath.row;
    [_tableView reloadData];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.f;
}
- (void)cancelOrSureEvent:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    NSUInteger index = btn.tag;
    
    if (index == 3002) {
        
        if (_type == TheElectricVoltage) {
            if (_vTextF.text.length >0) {
                _contentStr = _vTextF.text;
            }
        }
        if (_contentStr.length > 0) {
            _strBlock(_contentStr);
        }
    }

    [self zd_Windowclose];

}
#pragma mark -关闭
- (void)zd_Windowclose {
    [self dismissKeyBoard];
    [UIView animateWithDuration:0.5 delay:0.0 usingSpringWithDamping:0.5 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        self.zd_superView.center = CGPointMake(zd_width/2.0,-230);
        
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}
#pragma mark -UITextFieldDelegate
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldChanged:(NSNotification*)noti{
    
    UITextField *textField = (UITextField *)noti.object;
    
    BOOL flag=[NSString isContainsTwoEmoji:textField.text];
    if (flag){
        textField.text = [NSString disable_emoji:textField.text];
    }
    
    if ([ConFunc isPureInt:_vTextF.text] == NO) {
        _vTextF.text = [_vTextF.text substringToIndex:_vTextF.text.length - 1];
    }
    
    if (textField.text.length >0){
        _selectIndex = -1;
        [_tableView reloadData];
    }
    
}
#pragma mark -UIButtonEvent
- (void)dismissKeyBoard{
    [self endEditing:YES];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
