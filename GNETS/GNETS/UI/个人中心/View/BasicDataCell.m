//
//  BasicDataCell.m
//  GNETS
//
//  Created by fyc on 16/2/23.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "BasicDataCell.h"

@implementation BasicDataCell

- (void)awakeFromNib {
    // Initialization code
    self.detailTitleLa.adjustsFontSizeToFitWidth = YES;
    
    _lineView = [[UIView alloc] initWithFrame:CGRectMake(15, 43.5, [UIScreen mainScreen].bounds.size.width-15 , .5)];
    _lineView.backgroundColor = [UIColor colorWithHexString:@"#d7d7d7"];//RGBACOLOR(235, 235, 236, 1);//colorWithHexString:@"#FFA043"];
    [self.contentView addSubview:_lineView];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
