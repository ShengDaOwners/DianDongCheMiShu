//
//  InsuranceRegionCell.h
//  GNETS
//
//  Created by fyc on 16/2/18.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsuranceRegionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLa;
@property (strong, nonatomic)  UILabel *regionLa;

@end
