//
//  CarDataCell.m
//  GNETS
//
//  Created by fyc on 16/2/23.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "CarDataCell.h"

@implementation CarDataCell

- (void)awakeFromNib {
    // Initialization code
    
    _lineView = [[UIView alloc] initWithFrame:CGRectMake(15, 43.5, [UIScreen mainScreen].bounds.size.width-15 , .5)];
    _lineView.backgroundColor = [UIColor colorWithHexString:@"#d7d7d7"];//RGBACOLOR(235, 235, 236, 1);//colorWithHexString:@"#FFA043"];
    [self.contentView addSubview:_lineView];
    
    _detailLa = [[UILabel alloc] init];
    _detailLa.font = Font_14;
    _detailLa.textAlignment = NSTextAlignmentRight;
    _detailLa.textColor = [UIColor colorWithHexString:@"#666666"];
    _detailLa.frame = CGRectMake(kMainScreenWidth - 185, 12, 160, 20);
    [self.contentView addSubview:_detailLa];
    
    _picImgView = [[UIImageView alloc] initWithFrame:CGRectMake(kMainScreenWidth - 64, 9, 32, 25)];
    _picImgView.image = [UIImage imageNamed:@"icon_camera"];
    _picImgView.userInteractionEnabled = YES;
    [self.contentView addSubview:_picImgView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
