//
//  ControlTypeVC.h
//  GNETS
//
//  Created by cqz on 16/6/13.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "ViewController.h"
#import "BaseViewController.h"

@interface ControlTypeVC : BaseViewController


@property (nonatomic, copy) NSString *controlType; //远程控制
@property (nonatomic, copy) NSString *lockStr; //锁车状态

@end
