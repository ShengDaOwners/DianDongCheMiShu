//
//  _root_.h
//  电动车秘书
//
//  Created by _author on 16-02-28.
//  Copyright (c) _companyname. All rights reserved.
//

/*
	
*/


#import <Foundation/Foundation.h>
#import "DTApiBaseBean.h"
#import "LocInfodata.h"

@interface LocInfoModel : NSObject
{
	NSNumber *_code;
	LocInfodata *_data;
	NSString *_errmsg;
}


@property (nonatomic, copy) NSNumber *code;
@property (nonatomic, retain) LocInfodata *data;
@property (nonatomic, copy) NSString *errmsg;

-(id)initWithDictionary:(NSDictionary*)dict;
-(NSDictionary*)dictionaryValue;
@end
 