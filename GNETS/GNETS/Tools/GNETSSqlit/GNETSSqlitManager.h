//
//  GNETSSqlitManager.h
//  GNETS
//
//  Created by fyc on 16/3/1.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GNETSSqlitManager : NSObject
/*
 *创建数据库
 */
+ (void)copyOrCreateDB;
/*
 *获取国家列表
 */
+ (NSMutableArray *)getCountryList;
/*
 *获取省列表
 */
+ (NSMutableArray *)getProvinceListWithContryId:(NSString *)contryId;
/*
 *获取市列表
 */
+ (NSMutableArray *)getCityeListWithProvinceId:(NSString *)ProvinceId;
/*
 *获取地区列表
 */
+ (NSMutableArray *)getDistrictListWithCityId:(NSString *)CityId;
@end
