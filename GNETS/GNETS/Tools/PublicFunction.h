//
//  PublicFunction.h
//  ULife
//
//  Created by fengyixiao on 15/7/7.
//  Copyright (c) 2015年 UHouse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"

@interface PublicFunction : NSObject

@property(nonatomic)BOOL m_bLogin;
@property(nonatomic , strong)UserModel *m_user;
@property(nonatomic , copy)NSString *defaultDomain;
@property(nonatomic , copy)NSString *userToken;
+(PublicFunction *)ShareInstance;
//是否登录
- (BOOL)isLogin;
//取user信息
- (UserModel *)getAccount;
//存用户信息
- (void)loginSuccessWithAccount:(NSDictionary *)account;
//登出
- (void)logout;
//展示错误信息
+ (void)showErrorMsg:(NSDictionary *)errorMsg;
//获取某种时间格式的时间（nsdate）
+ (NSDate *)getDateWithDateStr:(NSString *)dateStr;
/*!
 *获取当前时间（nsstring）
 */
+ (NSString *)getCurrentDateStrWithDateFormmatter:(NSString *)formatter;
/*!
 *给一个 时间戳（毫秒）  ＋ 1天   转换成 2015-09-22格式
 */
+ (NSString*)getCustomTimeFormatWithScheduleStr:(NSString*)scheduleStr WithFormatter:(NSString*)formatter WithSecond:(int)second;
/*!
 *给一个 时间字符串（2015-09-22）＋1天 获取  2015-09-23格式
 */
+ (NSString*)getCustomTimeFormatWithDateStr:(NSString*)dateStr WithFormatter:(NSString*)formatter WithSchedule:(int)schedule;
@end
