//
//  PublicFunction.m
//  ULife
//
//  Created by fengyixiao on 15/7/7.
//  Copyright (c) 2015年 UHouse. All rights reserved.
//

#import "PublicFunction.h"

@implementation PublicFunction
+(PublicFunction *)ShareInstance
{
    static PublicFunction *hanle= nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        hanle = [[self alloc] init];
    });
    return hanle;
}   
-(BOOL)isFirstLaunch
{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
        NSLog(@"第一次启动");
        return YES;
    }
    else
    {
        NSLog(@"已经不是第一次启动了");
        return NO;
    }
}
//是否登录
- (BOOL)isLogin{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"];
}
//取user信息
- (UserModel *)getAccount{
    NSString *acountPath = [self getPathWithFileName:@"account.data"];
    NSData *accountData = [[NSData alloc] initWithContentsOfFile:acountPath];
    NSDictionary *userDic = [NSKeyedUnarchiver unarchiveObjectWithData:accountData];
    UserModel *user = [[UserModel alloc] initWithDictionary:userDic];
    return user;
}
//存用户信息
- (void)loginSuccessWithAccount:(NSDictionary *)account{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLogin"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *acountPath = [self getPathWithFileName:@"account.data"];
    NSData *accountData = [NSKeyedArchiver archivedDataWithRootObject:account];
    [accountData writeToFile:acountPath atomically:YES];
}
//登出
- (void)logout{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isLogin"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *acountPath = [self getPathWithFileName:@"account.data"];
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:acountPath error:&error];
    if (error) {
        NSLog(@"delete error : %@",error);
    }
}
- (NSString *)getPathWithFileName:(NSString *)fileName{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    return [path stringByAppendingPathComponent:fileName];
}
//展示错误信息
+ (void)showErrorMsg:(NSDictionary *)errorMsg{
    if ([errorMsg[@"code"] intValue] != 1 && ![errorMsg[@"errmsg"] isEqualToString:@"ok"]) {
        [SVProgressHUD showErrorWithStatus:errorMsg[@"errmsg"]];
        
        return;

    }
}
//获取当前时间（nsstring）
+ (NSString *)getCurrentDateStrWithDateFormmatter:(NSString *)formatter{
    NSDate *  senddate=[NSDate date];
    
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    
    //    [dateformatter setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"zh_CN"]];//location设置为中国
    
//    [dateformatter setDateFormat:@"yyyy-MM-dd"];
    dateformatter.dateFormat = formatter;
    
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    
    return locationString;
}
//获取某种时间格式的时间（nsdate） dateStr 时间字符串
+ (NSDate *)getDateWithDateStr:(NSString *)dateStr{
    NSDateFormatter *f1 = [[NSDateFormatter alloc] init];
    [f1 setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [f1 dateFromString:dateStr];
    
    return date;
}
/*!
 *给一个 时间戳（毫秒）  ＋ 1天   转换成 2015-09-22格式
 */
+ (NSString*)getCustomTimeFormatWithScheduleStr:(NSString*)scheduleStr WithFormatter:(NSString*)formatter WithSecond:(int)second{
    long long time = [scheduleStr longLongValue];
    NSDate *d= [[NSDate alloc]initWithTimeIntervalSince1970:time/1000.0];
    NSDate *d1 = [d dateByAddingTimeInterval:second];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = formatter;
    //   NSLog(@"adsasd===%@",dateFormatter.monthSymbols);
    return [dateFormatter stringFromDate:d1];
}
//给一个 时间字符串（2015-09-22）＋1天 获取  2015-09-23格式
+ (NSString*)getCustomTimeFormatWithDateStr:(NSString*)dateStr WithFormatter:(NSString*)formatter WithSchedule:(int)schedule{
    
    NSDateFormatter *f1 = [[NSDateFormatter alloc] init];
    [f1 setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [f1 dateFromString:dateStr];
    

    NSDate *d1 = [date dateByAddingTimeInterval:schedule];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = formatter;
    //   NSLog(@"adsasd===%@",dateFormatter.monthSymbols);
    return [dateFormatter stringFromDate:d1];
}

@end
