//
//  NSString+MD5Encrypt.h
//  AirSchool
//
//  Created by admin on 14-7-10.
//  Copyright (c) 2014年 chinasoft. All rights reserved.
//

#import "NSString+MD5.h"
#import "GTMBase64.h"

@implementation NSString (MD5)

- (id)md5
{
    const char *cStr = [self UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
}

- (NSString *)md5Encrypt {

    const char *cStr = [self UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, (int)strlen(cStr), result );
    return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

- (NSString *)base64Md5Encrypt
{
    CC_MD5_CTX md5;
    
    CC_MD5_Init(&md5);
    
    CC_MD5_Update(&md5, [[self dataUsingEncoding:NSUTF8StringEncoding] bytes], (int)[[self dataUsingEncoding:NSUTF8StringEncoding] length]);
    
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5_Final(digest, &md5);
    
    NSString *result = [NSString stringWithFormat:@"%@",[GTMBase64 stringByEncodingBytes:digest length:CC_MD5_DIGEST_LENGTH]];
    return result;
}
@end
