//
//  NSString+MD5Encrypt.h
//  AirSchool
//
//  Created by admin on 14-7-10.
//  Copyright (c) 2014年 chinasoft. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h>
#import <Foundation/Foundation.h>
@interface NSString (MD5)

- (NSString *) md5;
- (NSString *)md5Encrypt;

- (NSString *)base64Md5Encrypt;
@end
