//
//  GNETS_RequestDefine.h
//  GNETS
//
//  Created by tcnj on 16/2/17.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#ifndef GNETS_RequestDefine_h
#define GNETS_RequestDefine_h


#endif /* GNETS_RequestDefine_h */

#define kProjectBaseUrl @"http://api.gnets.cn/app/"

// 1登录
#define LOGINURL @"checkLogin.do"
// 2用户注册
#define REGISTERURL @"user/regDevice.do"
// 3获取用户信息
#define INFOURL @"user/getUserInfo.do"
// 4获取车辆的基本信息
#define CARINFO     @"user/getCarInfo.do"
// 5根据用户输入IMEI后八位自动补全IMEI
#define SearchCarLikeImei @"user/searchCarLikeImei.do"
// 6获取报警消息
#define AlarmEventInfo @"alarm/getNewAlarmEventInfo.do"
// 7获取每日统计数据
#define StaticSofEveryDay @"chart/getDayData.do"
// 8获取车辆的基本信息
#define CarDataInfo @"user/getCarInfo.do"
// 9获取指定天数的统计数据
#define StaticSectionDateInfo @"chart/getSomeDayData.do"
// 10获取车辆位置信息
#define CarLocationInfo @"map/getLocInfo.do"
// 11执行远程锁车命令
#define LockCar @"lock/lockBike.do"
// 12执行远程解锁命令
#define UnLockCar @"lock/unLockBike.do"
// 13获取用户保险信息
#define InsuranceInfo @"insur/getInsurInfo.do"
// 14执行关闭电子围栏
#define CLOSEVF                     @"vf/closeVf.do"
// 15退出登录
#define Logout                      @"logout.do"
// 16开启电子围栏
#define OPENVF                      @"vf/openVf.do"
// 17根据起止时间获取轨迹信息
#define SEARCHTRACK                 @"map/searchTrack.do"
// 18根据用户输入IMEI后八位自动补全IMEI
#define IMEIURLSTRING               @"user/searchCarLikeImei.do"
// 19查看报警消息
#define LookAlarmEvent              @"/alarm/viewAlarmEvent.do"
// 20在线预订
#define ONLINEBOOK                  @"book/saveOnlineBook.do"

/**
 *  修改车辆资料
 */
#define UpdateCar                   @"user/updateCarInfo.do"

/**
 *  修改基本资料
 */
#define UpdateUserInfo              @"user/updateUserInfo.do"

